package domain

import domain.ArticleOrigin.IMPORTED
import domain.ArticleOrigin.LOCAL
import domain.ArticleType.TAXABLE
import domain.utils.PriceFormatter.format
import kotlin.math.round
import kotlin.math.roundToInt

private const val IMPORTED_LABEL = "imported"
private const val TAX_ROUND_PRECISION = 0.05

data class Article(private val name: String,
                   private val price: Double,
                   private val type: ArticleType = TAXABLE,
                   private val origin: ArticleOrigin = LOCAL)
{
    fun getPriceIncludingTax() = price + getTotalVAT()

    fun getTotalVAT() = getBasicTax() + getImportedTax()

    private fun getBasicTax(): Double = (price * type.taxRate).roundUpToNearestPrecision()

    private fun getImportedTax(): Double = (price * origin.taxRate).roundUpToNearestPrecision()

    private fun Double.roundUpToNearestPrecision(): Double =
            (round(this / TAX_ROUND_PRECISION) * TAX_ROUND_PRECISION).roundTo2DecimalPlaces()

    private fun Double.roundTo2DecimalPlaces() = (this * 100).roundToInt() / 100.0

    override fun toString() = "${completeName()}: ${format(getPriceIncludingTax())}"
    private fun completeName() = if (origin == IMPORTED) "$IMPORTED_LABEL $name" else name
}