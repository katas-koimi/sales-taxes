package domain

class Basket
{
    private val content = mutableMapOf<Article, Int>()

    fun add(vararg items: Pair<Article, Int>)
    {
        items.forEach { item ->
            if (exists(item))
            {
                updateQuantity(item)
            }
            else
            {
                content[item.first] = item.second
            }
        }
    }

    private fun exists(item: Pair<Article, Int>) = content[item.first] != null

    private fun updateQuantity(item: Pair<Article, Int>)
    {
        content[item.first] = content[item.first]!! + item.second
    }

    fun getTotalPrice() = content.map { (article, quantity) -> article.getPriceIncludingTax() * quantity }.sum()

    fun getTotalTaxes() = content.map { (article, quantity) -> article.getTotalVAT() * quantity }.sum()

    fun getDetails(): String
    {
        val details = StringBuilder()
        content.forEach { (article, quantity) ->
            val value = "$quantity $article"
            details.appendLine(value)
        }
        return details.toString().trim()
    }
}