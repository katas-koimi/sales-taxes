package domain

enum class ArticleType(val taxRate: Double)
{
    BOOK(0.0),
    FOOD(0.0),
    MEDICAL(0.0),

    TAXABLE(0.1);
}