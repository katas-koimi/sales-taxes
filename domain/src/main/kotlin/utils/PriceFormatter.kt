package domain.utils

import java.util.*

object PriceFormatter
{
    fun format(value: Double) = String.format(Locale.US, "%.2f", value)
}
