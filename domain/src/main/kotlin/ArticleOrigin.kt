package domain

enum class ArticleOrigin(val taxRate: Double)
{
    IMPORTED(0.05),
    LOCAL(0.0)
}