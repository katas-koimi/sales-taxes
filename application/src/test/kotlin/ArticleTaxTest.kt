import domain.Article
import domain.ArticleOrigin.IMPORTED
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class ArticleTaxTest
{
    @Test
    fun `tax is rounded up to the nearest 0,05`()
    {
        val article = Article("dummy", 47.5, origin = IMPORTED)

        assertEquals(54.65, article.getPriceIncludingTax())
    }
}