import application.PrintReceipt
import domain.Article
import domain.ArticleOrigin.IMPORTED
import domain.ArticleType.*
import domain.Basket
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class PrintReceiptTest
{
    private lateinit var basket: Basket

    @BeforeEach
    fun setup()
    {
        basket = Basket()
    }

    @Test
    fun `given an article exempt of tax then no tax applied`()
    {
        val book = Article("book", 12.49, BOOK)

        basket.add(book to 1)

        assertEquals(
            """
            1 book: 12.49
            Sales Taxes: 0.00
            Total: 12.49
        """.trimIndent(), PrintReceipt(basket).execute()
        )
    }

    @Test
    fun `given an article exempt of tax then no tax applied (other price)`()
    {
        val book = Article("book", 457.0, BOOK)

        basket.add(book to 1)

        assertEquals(
            """
            1 book: 457.00
            Sales Taxes: 0.00
            Total: 457.00
        """.trimIndent(), PrintReceipt(basket).execute()
        )
    }

    @Test
    fun `given an article exempt of tax then no tax applied (other name)`()
    {
        val food = Article("food", 5.5, FOOD)

        basket.add(food to 1)

        assertEquals(
            """
            1 food: 5.50
            Sales Taxes: 0.00
            Total: 5.50
        """.trimIndent(), PrintReceipt(basket).execute()
        )
    }

    @Test
    fun `given an article exempt of tax then no tax applied (other quantity)`()
    {
        val food = Article("food", 5.5, FOOD)

        basket.add(food to 2)

        assertEquals(
            """
            2 food: 5.50
            Sales Taxes: 0.00
            Total: 11.00
        """.trimIndent(), PrintReceipt(basket).execute()
        )
    }

    @Test
    fun `given different articles exempt of tax then no tax applied`()
    {
        val book = Article("book", 5.5, BOOK)
        val food = Article("food", 10.0, FOOD)

        basket.add(
            book to 1,
            food to 1
        )

        assertEquals(
            """
            1 book: 5.50
            1 food: 10.00
            Sales Taxes: 0.00
            Total: 15.50
        """.trimIndent(), PrintReceipt(basket).execute()
        )
    }

    @Test
    fun `given different articles exempt of tax then no tax applied (other case)`()
    {
        val hotChocolate = Article("hot chocolate", 6.50, FOOD)
        val chickenFries = Article("chicken fries", 2.99, FOOD)
        val medicine = Article("medicine", 14.50, MEDICAL)

        basket.add(
            hotChocolate to 6,
            chickenFries to 5,
            medicine to 2
        )

        assertEquals(
            """
            6 hot chocolate: 6.50
            5 chicken fries: 2.99
            2 medicine: 14.50
            Sales Taxes: 0.00
            Total: 82.95
        """.trimIndent(), PrintReceipt(basket).execute()
        )
    }

    @Test
    fun `given article with tax then tax is applied`()
    {
        val musicCD = Article("music CD", 14.99)

        basket.add(musicCD to 1)

        assertEquals(
            """
            1 music CD: 16.49
            Sales Taxes: 1.50
            Total: 16.49
        """.trimIndent(), PrintReceipt(basket).execute()
        )
    }

    @Test
    fun `given article with tax then tax is applied (other quantity)`()
    {
        val musicCD = Article("music CD", 14.99)

        basket.add(musicCD to 5)

        assertEquals(
            """
            5 music CD: 16.49
            Sales Taxes: 7.50
            Total: 82.45
        """.trimIndent(), PrintReceipt(basket).execute()
        )
    }

    @Test
    fun `melting pot with all type of articles`()
    {
        val book = Article("book", 12.49, BOOK)
        val musicCD = Article("music CD", 14.99)
        val chocolateBar = Article("chocolate bar", 0.85, FOOD)

        basket.add(
            book to 1,
            musicCD to 1,
            chocolateBar to 1
        )

        assertEquals(
            """
            1 book: 12.49
            1 music CD: 16.49
            1 chocolate bar: 0.85
            Sales Taxes: 1.50
            Total: 29.83
        """.trimIndent(), PrintReceipt(basket).execute()
        )
    }

    @Test
    fun `given imported article exempt of tax then import tax is applied`()
    {
        val medical = Article("doliprane", 4.0, MEDICAL, IMPORTED)

        basket.add(medical to 2)

        assertEquals(
            """
            2 imported doliprane: 4.20
            Sales Taxes: 0.40
            Total: 8.40
        """.trimIndent(), PrintReceipt(basket).execute()
        )
    }

    @Test
    fun `given imported article exempt of tax then import tax is applied (other case)`()
    {
        val chocolate = Article("box of chocolates", 10.0, FOOD, IMPORTED)
        val perfume = Article("bottle of perfume", 47.5, origin = IMPORTED)

        basket.add(
            chocolate to 1,
            perfume to 3
        )

        assertEquals(
            """
            1 imported box of chocolates: 10.50
            3 imported bottle of perfume: 54.65
            Sales Taxes: 21.95
            Total: 174.45
        """.trimIndent(), PrintReceipt(basket).execute()
        )
    }

    @Test
    fun `melting pot of all type of article from all origin`()
    {
        val importedPerfume = Article("bottle of perfume", 27.99, origin = IMPORTED)
        val perfume = Article("bottle of perfume", 18.99)
        val pills = Article("packet of headache pills", 9.75, MEDICAL)
        val chocolate = Article("box of chocolates", 11.25, FOOD, IMPORTED)

        basket.add(
            importedPerfume to 1,
            perfume to 1,
            pills to 1,
            chocolate to 1
        )

        assertEquals(
            """
            1 imported bottle of perfume: 32.19
            1 bottle of perfume: 20.89
            1 packet of headache pills: 9.75
            1 imported box of chocolates: 11.80
            Sales Taxes: 6.65
            Total: 74.63
        """.trimIndent(), PrintReceipt(basket).execute()
        )
    }

    @Test
    fun `adding the same article several times increases its quantity`()
    {
        val sushi = Article("Sushi", 12.8, FOOD)

        basket.add(
            sushi to 1,
            sushi to 2
        )

        assertEquals(
            """
            3 Sushi: 12.80
            Sales Taxes: 0.00
            Total: 38.40
        """.trimIndent(), PrintReceipt(basket).execute()
        )
    }
}