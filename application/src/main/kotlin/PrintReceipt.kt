package application

import domain.Basket
import domain.utils.PriceFormatter.format

class PrintReceipt(private val basket: Basket)
{
    fun execute(): String
    {
        val receipt = StringBuilder()
        receipt.appendLine(basket.getDetails())
        receipt.append(
            """
            Sales Taxes: ${format(basket.getTotalTaxes())}
            Total: ${format(basket.getTotalPrice())}
        """.trimIndent()
        )

        return receipt.toString()
    }
}
